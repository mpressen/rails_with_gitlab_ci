require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  if ENV['SELENIUM_REMOTE_URL']
    Capybara.server_host = '0.0.0.0'
    driven_by :selenium, using: :headless_chrome, screen_size: [800, 800], options: { url: ENV['SELENIUM_REMOTE_URL'] }
  else
    driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
  end

  def setup
    if ENV['SELENIUM_REMOTE_URL']
      net = Socket.ip_address_list.detect(&:ipv4_private?)
      ip = net.nil? ? 'localhost' : net.ip_address
      Capybara.app_host = "http://#{ip}"
    end
    super
  end
end
